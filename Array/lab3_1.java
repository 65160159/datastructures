
import java.util.Scanner;
public class lab3_1 {
    public static int mySqrt(int x) {
        if (x == 0) {
            return 0;
        }

        int check = 1;int x2 = x;int result = 0;

        while (check <= x2) {
            int ans = check + (x2 - check) / 2;

            if (ans * ans <= x) {
                result = ans;
                check = ans + 1;
            } else {
                x2 = ans - 1;
            }
        }
        return (int) result;
    }

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.print("input number: ");
        int x = kb.nextInt();
        int result = mySqrt(x);
        System.out.println("Square root of " + x + " is " + result);
    }
}

import java.util.Arrays;
public class lab1_1 {
    public static void main(String[] args) {
        // Declare and initialize the "numbers" array
        int[] numbers = { 5, 8, 3, 2, 7 };

        // Declare and initialize the "names" array
        String[] names = { "Alice", "Bob", "Charlie", "David" };

        // Declare and initialize the "values" array with 4 elements
        double[] values = new double[4];

        // Print the elements of the "numbers" array using a for loop
        System.out.println("Numbers array:");
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }
        System.out.println();

        // Print the elements of the "names" array using a for-each loop
        System.out.println("Names array:");
        for (String name : names) {
            System.out.print(name + " ");
        }
        System.out.println();

        // Initialize the elements of the "values" array
        values[0] = 3.14;
        values[1] = 2.3;
        values[2] = 3.78;
        values[3] = 5.21;

        // Calculate and print the sum of all elements in the "numbers" array
        int sum = 0;
        for (int number : numbers) {
            sum += number;
        }
        System.out.println("Sum of numbers: " + sum);

        // Find and print the maximum value in the "values" array
        double max = values[0];
        for (int i = 1; i < values.length; i++) {
            if (values[i] > max) {
                max = values[i];
            }
        }
        System.out.println("Maximum value in the 'values' array: " + max);

        // Create a new string array "reversedNames" with the same length as "names"
        // array
        String[] reversedNames = new String[names.length];

        // Fill the "reversedNames" array with the elements of the "names" array in
        // reverse order
        for (int i = 0; i < names.length; i++) {
            reversedNames[i] = names[names.length - 1 - i];
        }

        // Print the elements of the "reversedNames" array
        System.out.println("Reversed Names array:");
        for (String reversedName : reversedNames) {
            System.out.print(reversedName + " ");
        }
        System.out.println();

        // BONUS: Sort the "numbers" array in ascending order (using Arrays.sort)
        Arrays.sort(numbers);

        // Print the sorted "numbers" array
        System.out.println("Sorted Numbers array:");
        for (int number : numbers) {
            System.out.print(number + " ");
        }
        
        
    }
}
